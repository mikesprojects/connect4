//
//  main.swift
//  Connect4
//
//  Created by Michael Aubie on 07/10/2016.
//  Copyright © 2016 Michael Aubie. All rights reserved.
//

import Foundation

var userMap: [String] = [
    "#################",
    "#               #",
    "#               #",
    "#               #",
    "#               #",
    "#               #",
    "#               #",
    "#               #",
    "#---------------#",
    "#####1234567######"]


let mapY = userMap.count
let mapX = userMap[0].characters.count

var gameMap = Array<Array<Character>>()

//initialize map array
for _ in 0..<mapY {
    gameMap.append(Array(repeating:Character("A"), count:mapX))
}

//set gameMap Character array equal to the map string array
for y in 0...mapY - 1{
    let tempMapLine = userMap[y].characters.map{ String($0) }
    
    for x in 0...mapX-1 {
        gameMap[y][x] = Character(tempMapLine[x])
    }
}

//initialize the game class
var myGame = ConnectFour(playableX: 7,playableY: 6, theMap: gameMap)

//game loop
while myGame.isGameOver() == false{
    myGame.updateTheBoard()
    myGame.printTheBoard()
    
    myGame.askForInput(playerNumber: 1)
    if myGame.isGameOver() == true {
        break
    }
    myGame.updateTheBoard()
    myGame.printTheBoard()
    
    myGame.askForInput(playerNumber: 2)
    myGame.checkForWin()
}
myGame.updateTheBoard()
myGame.printTheBoard()
print("GAME OVER")





