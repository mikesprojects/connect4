//
//  ConnectFour.swift
//  Connect4
//
//  Created by Michael Aubie on 08/10/2016.
//  Copyright © 2016 Michael Aubie. All rights reserved.
//

import Foundation

enum PositionPlayed:Int{
    case Empty = 0
    case Player1 = 1
    case Player2 = 2
}
class ConnectFour{
    var playableColumns = Array<Array<Character>>()
    var playableMapX:Int
    var playableMapY:Int
    var gameOver:Bool
    
    var Player1 = 1
    var Player2 = 2
    var theboard = Array<Array<Character>>()
    
    init(playableX:Int, playableY:Int, theMap:[[Character]])
    {
        for _ in 0..<playableY {
            playableColumns.append(Array(repeating:Character(" "), count: playableX))
        }
        
        playableMapX = playableX
        playableMapY = playableY
        theboard = theMap
        gameOver = false
    }
    func isGameOver()->Bool{
        return gameOver
    }
    
    func askForInput(playerNumber:Int){
        var validInput = false
        while validInput == false{
            print(String(format: "Player: %i Choose a column: 1-7 or 0 to quit", playerNumber))
            let userInput:Int? = Int(readLine()!)
            if userInput != nil {
                if ChoiceIsOk(playerChoice: userInput!) == true && gameOver == false{
                    addToken(columnChoice: userInput!, playerNumber: playerNumber)
                    validInput = true
                }
                if gameOver == true {
                    validInput = true
                }
            }
        }
    }
    
    func addToken(columnChoice:Int, playerNumber:Int)
    {
        for i in 0..<playableMapY{
            if playableColumns[i][columnChoice-1] == " "{
                if playerNumber == 1 { playableColumns[i][columnChoice-1] = "X"}
                if playerNumber == 2 {playableColumns[i][columnChoice-1] = "O"}
                break
            }
        }
        if checkForWin() == true {
            print(String(format: "Player %@ won", playerNumber))
        }
    }
    
    func ChoiceIsOk(playerChoice:Int) -> Bool{
        if playerChoice == 0 {
            gameOver = true
            print("Player Quit")
        }
        //did the player choose a valid column with an empty space in it?
        if playerChoice >= playableMapY || playerChoice < 1 { return false }
        if playableColumns[playableMapY-1][playerChoice-1].description == " "{
            return true
        }
        return false
    }
    
    func printTheBoard(){
        for y in 0..<mapY{
            print(String(theboard[y]))
        }
    }
    
    func updateTheBoard(){
        for y in 0..<playableMapY{
            for x in 0..<playableMapX{
                theboard[playableMapY-y+1][x+5] = playableColumns[y][x]
            }
        }
    }
    func checkForWin()->Bool {

        for a in 0..<playableMapY{
            for b in 0..<playableMapX-4{    //4 to win
                
                if playableColumns[a][b] == "X" &&
                    playableColumns[a][b+1] == "X" &&
                    playableColumns[a][b+2] ==  "X" &&
                    playableColumns[a][b+3] == "X" {
                    print("player1 won")
                    gameOver = true
                }
                if playableColumns[a][b] == "O" &&
                    playableColumns[a][b+1] == "O" &&
                    playableColumns[a][b+2] ==  "O" &&
                    playableColumns[a][b+3] == "O" {
                    print("player2 won")
                    gameOver = true
                }
            }
        }
        //checks the Y
        for a in 0..<playableMapX{
            for b in 0..<playableMapY-4{
                if playableColumns[b][a] == "X" &&
                    playableColumns[b+1][a] == "X" &&
                    playableColumns[b+2][a] == "X" &&
                    playableColumns[b+3][a] == "X" {
                    print("player1 wins")
                    gameOver = true
                }
                if playableColumns[b][a] == "O" &&
                    playableColumns[b+1][a] == "O" &&
                    playableColumns[b+2][a] == "O" &&
                    playableColumns[b+3][a] == "O" {
                    print("player2 wins")
                    gameOver = true
                }
            }
        }
        
        //check diagonal
        for a in 0...playableMapX-3{
            for b in stride(from:playableMapY-1, to: 2, by: -1){
                if playableColumns[b][a] == "X" &&
                    playableColumns[b-1][a+1] == "X" &&
                    playableColumns[b-2][a+2] == "X" &&
                    playableColumns[b-3][a+3] == "X" {
                    print("Player1 wins")
                    gameOver = true
                }
                if playableColumns[b][a] == "O" &&
                    playableColumns[b-1][a+1] == "O" &&
                    playableColumns[b-2][a+2] == "O" &&
                    playableColumns[b-3][a+3] == "O" {
                    print("Player2 wins")
                    gameOver = true
                }
            }
        }
        
        //check diagonal
        for a in stride(from:playableMapX-1, to: 2, by: -1){
            for b in stride(from:playableMapY-1, to:2, by: -1){
                if playableColumns[b][a-1] == "X" &&
                    playableColumns[b-1][a-2] == "X" &&
                    playableColumns[b-2][a-3] == "X" {
                    print("Player1 wins")
                    gameOver = true
                }
                if  playableColumns[b][a] == "O" &&
                    playableColumns[b-1][a-1] == "O" &&
                    playableColumns[b-2][a-2] == "O" &&
                    playableColumns[b-3][a-3] == "O" {
                    print("Player2 wins")
                    gameOver = true
                }
            }
        }
        return false
    }
}
